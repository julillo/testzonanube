package com.jlillo.testzonanube.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jlillo.testzonanube.model.Campos;

@Controller
@RequestMapping("/test")
public class TestController {

	private static final String INDEX = "index";
	private static final Log LOG = LogFactory.getLog(TestController.class);

	@GetMapping("/")
	public String redirect() {
		return "redirect:/test/index";
	}

	@GetMapping("/index")
	public ModelAndView index(@RequestParam(name = "json", required = false, defaultValue = "NULL") String json) {
		LOG.info(json);
		ModelAndView mav = new ModelAndView(INDEX);
		List<Campos> elementos = new ArrayList<>();
		try {
			JsonElement jelement = new JsonParser().parse(json);
			JsonObject jobject = jelement.getAsJsonObject();
			JsonArray jarray = jobject.getAsJsonArray("ListaMarcadores");

			for (int i = 0; i < jarray.size(); i++) {
				elementos.add(new Campos.Builder()
						.key(jarray.get(i).getAsJsonObject().get("Marcador").toString().replace("<", "")
								.replace(">", "").trim())
						.value(jarray.get(i).getAsJsonObject().get("valor").toString().trim()).build());
			}
			LOG.info(elementos.toString());
		} catch (Exception ex) {
			LOG.info(ex.toString());
		}
		mav.addObject("elementos", elementos);
		return mav;
	}
}
