package com.jlillo.testzonanube.model;

public class Campos {
	
	private String key;
	private String value;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public static class Builder{
		private String _key;
		private String _value;
		
		public Builder key(String _key) {
			this._key = _key;
			return this;
		}
		
		public Builder value(String _value) {
			this._value = _value;
			return this;
		}
		
		public Campos build() {
			return new Campos(this);
		}
	}
	
	private Campos(Builder b) {
		this.key = b._key;
		this.value = b._value;
	}
	
	public Campos() {}
	
}
