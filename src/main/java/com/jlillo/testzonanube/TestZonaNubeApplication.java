package com.jlillo.testzonanube;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestZonaNubeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestZonaNubeApplication.class, args);
	}
}
